<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Zadania */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zadania-form">

    <?php $form = ActiveForm::begin(); ?>

   

    <?= $form->field($model, 'id_uzytkownika')->textInput() ?>

    <?= $form->field($model, 'tytul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'termin_rozpoczecia')->textInput() ?>

    <?= $form->field($model, 'termin_zakonczenia')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
