<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ZadaniaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zadania-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_zadania') ?>

    <?= $form->field($model, 'id_uzytkownika') ?>

    <?= $form->field($model, 'tytul') ?>

    <?= $form->field($model, 'opis') ?>

    <?= $form->field($model, 'termin_rozpoczecia') ?>

    <?php  echo $form->field($model, 'termin_zakonczenia') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
