<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "zadania".
 *
 * @property integer $id_zadania
 * @property integer $id_uzytkownika
 * @property string $tytul
 * @property string $opis
 * @property string $termin_rozpoczecia
 * @property string $termin_zakonczenia
 *
 * @property User $idUzytkownika
 */
class Zadania extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zadania';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_uzytkownika', 'tytul', 'opis', 'termin_rozpoczecia', 'termin_zakonczenia'], 'required'],
            [['id_uzytkownika'], 'integer'],
            [['termin_rozpoczecia', 'termin_zakonczenia'], 'safe'],
            [['tytul', 'opis'], 'string', 'max' => 100],
            [['id_uzytkownika'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_uzytkownika' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_zadania' => 'Id Zadania',
            'id_uzytkownika' => 'Id Uzytkownika',
            'tytul' => 'Tytul',
            'opis' => 'Opis',
            'termin_rozpoczecia' => 'Termin Rozpoczecia',
            'termin_zakonczenia' => 'Termin Zakonczenia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUzytkownika()
    {
        return $this->hasOne(User::className(), ['id' => 'id_uzytkownika']);
    }
}
