<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Zadania;

/**
 * ZadaniaSearch represents the model behind the search form about `frontend\models\Zadania`.
 */
class ZadaniaSearch extends Zadania
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_zadania', 'id_uzytkownika'], 'integer'],
            [['tytul', 'opis', 'termin_rozpoczecia', 'termin_zakonczenia'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Zadania::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_zadania' => $this->id_zadania,
            'id_uzytkownika' => $this->id_uzytkownika,
            'termin_rozpoczecia' => $this->termin_rozpoczecia,
            'termin_zakonczenia' => $this->termin_zakonczenia,
        ]);

        $query->andFilterWhere(['like', 'tytul', $this->tytul])
            ->andFilterWhere(['like', 'opis', $this->opis]);

        return $dataProvider;
    }
}
