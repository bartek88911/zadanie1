 # Zadanie do zrobienia:

## Opis

Zadanie polega na stworzeniu aplikacji do obsługi zadań. Aplikacja musi
być wykonana w języku PHP >=5.6. Projekt powinien zostać umieszczony w 
serwisie Gitlab - https://gitlab.com/users/sign_in

## Funkcjonalności

- Logowanie użytkowników
- Zadania powinny być przypisane do zalogowanego użytkownika i
niedostępne przez pozostałych
- Dodawanie/usuwanie zadań (pola: tytuł, opis, termin rozpoczęcia,
termin zakończenia)
- Wykorzystanie w aplikacji bootstrap 3


## Opcjonalnie
- Rejestracja użytkowników,
- Wyświetlanie zadań na kalendarzu,
